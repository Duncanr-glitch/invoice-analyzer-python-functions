"""Matches the location string to the location dict"""

from typing import TypedDict, Union


class LocationFullAbbreviate(TypedDict, total=False):
    """Type for location having full and abbfreviation"""

    full_name: str
    abbreviation: str


class LocationDict(TypedDict, total=False):
    """Dictionary type for a split location"""

    city: str
    state: LocationFullAbbreviate | str
    country: LocationFullAbbreviate | str
    address: str


def location_matching(
    location_string: str, location_dict: LocationDict
) -> Union[LocationDict, None]:
    """Matches the lowest level possible of the location dictionary to the location string"""
    for locale in location_dict:
        locale_name = location_dict.get(locale, "")
        if locale == "city" or isinstance(locale_name, str):
            if locale_name in location_string:
                return {locale: locale_name}  # type: ignore[reportGeneralTypeIssues]
        else:
            full_name = locale_name.get("full_name", "")
            abbreviation = locale_name.get("abbreviation", "")
            if (full_name and full_name in location_string) or (
                abbreviation and abbreviation in location_string
            ):
                return {locale: full_name or abbreviation}  # type: ignore[reportGeneralTypeIssues]
    return None
