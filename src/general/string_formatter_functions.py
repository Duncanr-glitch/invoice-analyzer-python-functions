# type: ignore[reportInvalidStringEscapeSequence]
"""Functions for formatting strings"""
import re


def format_cateogry_str(strip_str: str) -> str:
    """Formats and sanitizes category strings:
    1. Strips the spaces from the edges of the string.
    2. Makes the string fully lowercase.
    3. Splits mashed together categories.
    4. Correct reversed order.
    5. Depluarlizes categories.
    5. Turns it into camelCase."""

    # Excel categories
    categories = [
        "location",
        "date",
        "vendor",
        "category",
        "possible vendor categories",
        "code",
        "from",
        "until",
        "possible",
        "name",
    ]

    # Strips the trailing whitespace
    # Makes the string lowercase
    # Removes repeated spaces
    strip_str = re.sub("\s+", " ", strip_str.strip().lower())

    # Loops through the categories
    for category in categories:
        # Length of the current category
        category_len = len(category)

        # Remove any plurals from possible categories
        if strip_str in [
            "possible vendor category",
            "possible vendor categories",
            "possible vendors category",
            "possible vendors categories",
            "possible category vendor",
            "possible category vendors",
            "possible categories vendor",
            "possible categories vendors",
            "vendor possible category",
            "vendor possible categories",
            "vendor category possible",
            "vendor categories possible",
            "vendors possible category",
            "vendors possible categories",
            "vendors category possible",
            "vendors categories possible",
            "category possible vendor",
            "category possible vendors",
            "category vendor possible",
            "category vendors possible",
            "categories possible vendor",
            "categories possible vendors",
            "categories vendor possible",
            "categories vendors possible",
        ]:
            strip_str = "possible vendor categories"
        # Find any possible singular and plural combinations of date range name
        # and convert it to date
        elif strip_str in [
            "date range names",
            "date range name",
            "date ranges names",
            "date ranges name",
            "date names range",
            "date names ranges",
            "date name range",
            "date name ranges",
            "dates range names",
            "dates range name",
            "dates ranges names",
            "dates ranges name",
            "dates names range",
            "dates names ranges",
            "dates name range",
            "dates name ranges",
            "range date names",
            "range date name",
            "range dates names",
            "range dates name",
            "range names date",
            "range names dates",
            "range name date",
            "range name dates",
            "ranges date names",
            "ranges date name",
            "ranges dates names",
            "ranges dates name",
            "ranges names date",
            "ranges names dates",
            "ranges name date",
            "ranges name dates",
            "names date range",
            "names date ranges",
            "names dates range",
            "names dates ranges",
            "names range date",
            "names range dates",
            "names ranges date",
            "names ranges dates",
            "name date range",
            "name date ranges",
            "name dates range",
            "name dates ranges",
            "name range date",
            "name range dates",
            "name ranges date",
            "name ranges dates",
        ]:
            strip_str = "date"
        # Make all other plurals singular
        elif category == "category":
            strip_str = strip_str.replace("categories", "category")
        else:
            strip_str = strip_str.replace(f"{category}s", category)

        # Add a space if the category value parts aren't space-separated
        if re.match(f"{category}\S", strip_str):
            strip_str = f"{category} {strip_str[category_len:]}"

        # Reverse the category value parts if they are backwards
        if category in strip_str and strip_str not in categories and " " in strip_str:
            strip_str = " ".join(list(reversed(strip_str.split(" "))))

    # Clean any accidentally added trailing spaces
    strip_str = strip_str.strip()

    # Turn the categories into camelCase
    strip_str = "".join(
        [
            w.capitalize() if index > 0 else w
            for index, w in enumerate(re.split("\s+", strip_str))
        ]
    )
    # strip_str = strip_str.replace("dateRangeName", "date")
    return strip_str
