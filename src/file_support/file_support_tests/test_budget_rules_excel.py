"""Unittets for the excel budget rules functions"""
import unittest
from random import randint

from datetime import datetime, timedelta

from file_support.budget_rules_excel import (
    DateFromUntil,
    RulesValuesDict,
    date_value_scans,
    set_current_previous,
    set_previous_values,
)
from general.date_range import DateRange


class TestSetPrevValues(unittest.TestCase):
    """Test cases for setting the previous values field"""

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.existing_value = "test"
        self.previous_existing = ["first", "second"]
        self.basic_existing_data: RulesValuesDict = {
            "currentValue": self.existing_value,
            "previousValues": self.previous_existing,
        }

    def test_basic_full_dict(self):
        """Test when the dict has both current and previous value"""

        new_value = "new string"
        new_data = set_previous_values(new_value, self.basic_existing_data)

        expected_new_data = [self.existing_value] + self.previous_existing

        self.assertListEqual(expected_new_data, new_data)

    def test_new_value_in_prev(self):
        """Tests that the new value is no longer in the previous"""
        new_value = "second"
        new_data = set_previous_values(new_value, self.basic_existing_data)

        expected_new_data = ["test", "first"]
        self.assertListEqual(expected_new_data, new_data)

    def test_new_value_equal_existing(self):
        """Tests that previous values doesn't change with same value."""
        new_data = set_previous_values(self.existing_value, self.basic_existing_data)

        self.assertListEqual(self.previous_existing, new_data)

    def test_tenth_value_bumped(self):
        """Tests that when an eleventh value is added the last is bumped"""
        full_existing_data: RulesValuesDict = {
            "currentValue": self.existing_value,
            "previousValues": self.previous_existing + [i for i in range(8)],
        }
        new_data = set_previous_values("full", full_existing_data)

        expected_data = [self.existing_value] + full_existing_data["previousValues"][:9]
        self.assertListEqual(expected_data, new_data)

    def test_current_value_is_none(self):
        """Tests that when the current value is none, the previous value is unchanged"""
        new_data = set_previous_values(
            "anything", {"previousValues": self.previous_existing}
        )
        self.assertListEqual(self.previous_existing, new_data)

    def test_current_value_is_list(self):
        """Tests that list is properly stringified before going into previous"""
        current_lst = [1, True]
        new_data = set_previous_values(
            "some string",
            {"currentValue": current_lst, "previousValues": self.previous_existing},
        )
        self.assertListEqual([str(current_lst)] + self.previous_existing, new_data)

    def test_new_dict_in_list_replaces(self):
        """Tests that when a dict is the new value and its already in previous, it gets removed."""
        new_existing_dict = {1: 3, 3: 4}
        dict_replace_data: RulesValuesDict = {
            "currentValue": self.existing_value,
            "previousValues": self.previous_existing + [str(new_existing_dict)],
        }
        new_data = set_previous_values(new_existing_dict, dict_replace_data)
        self.assertListEqual([self.existing_value] + self.previous_existing, new_data)


class TestDateValueScand(unittest.TestCase):
    """Test cases for collating date values"""

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.output_format = "%Y-%m-%d"
        self.today = datetime.today()
        self.rand_from = self.today - timedelta(randint(1, 50))
        self.rand_until = self.today + timedelta(randint(1, 50))

        self.from_until_dict: DateFromUntil = {
            "dateFrom": [self.today, "24-12-01", "wfawef", None, self.rand_from],
            "dateUntil": [self.rand_until, "2025/3/31", 123, None],
        }

        self.current_date_range = DateRange.strpdaterange(
            "12-31-21,06-08-25", "%m-%d-%y"
        ).strftime(self.output_format)

        self.existing_date_range: RulesValuesDict = {
            "currentValue": self.current_date_range,
            "previousValues": [],
        }
        self.new_previous = [self.current_date_range]

    def test_basic_from_until(self):
        """Tests when there is valid from and until datetime with valid datetimes"""
        from_until = date_value_scans(self.from_until_dict, 0, self.existing_date_range)
        expected_result: RulesValuesDict = {
            "currentValue": f"{self.today.strftime(self.output_format)},{self.rand_until.strftime(self.output_format)}",
            "previousValues": self.new_previous,
        }
        self.assertDictEqual(expected_result, from_until)

    def test_valid_stringed_dates(self):
        """Tests sending valid datestrings"""
        from_until = date_value_scans(self.from_until_dict, 1, self.existing_date_range)
        expected_result: RulesValuesDict = {
            "currentValue": "2024-12-01,2025-03-31",
            "previousValues": self.new_previous,
        }
        self.assertDictEqual(expected_result, from_until)

    def test_invalid_stringed_dates(self):
        """Test sending invalid date values"""
        from_until = date_value_scans(self.from_until_dict, 2, self.existing_date_range)
        expected_result: RulesValuesDict = {
            "currentValue": "wfawef,123",
            "previousValues": self.new_previous,
        }
        self.assertDictEqual(expected_result, from_until)

    def test_none_in_from_until(self):
        """Tests when there's None set in dateFrom and dateUntil"""
        from_until = date_value_scans(self.from_until_dict, 3, self.existing_date_range)
        self.assertIsNone(from_until)

    def test_missing_until(self):
        """Tests when there's a from but no until at the list index"""
        from_until = date_value_scans(self.from_until_dict, 4, self.existing_date_range)
        self.assertIsNone(from_until)

    def test_add_code_false(self):
        """Tests when add code is false"""
        from_until = date_value_scans(
            self.from_until_dict, 0, self.existing_date_range, add_codes=False
        )
        expected_result: RulesValuesDict = {
            "currentValue": None,
            "previousValues": self.new_previous,
        }
        self.assertDictEqual(expected_result, from_until)


class TestSetCurrentPrevValues(unittest.TestCase):
    """Tests the current previous setting utility function"""

    def test_real_new_val(self):
        """Tests real new val and real existing"""
        new_current_prev = set_current_previous("something", ["existing"])
        expected_result: RulesValuesDict = {
            "currentValue": "something",
            "previousValues": ["existing"],
        }
        self.assertDictEqual(expected_result, new_current_prev)

    def test_none_new(self):
        """Tests when there is no new value"""
        new_none = set_current_previous(None, ["some previous"])
        self.assertIsNone(new_none)

    def test_none_no_previous(self):
        """Tests when there's a none new value and no previous"""
        none_added = set_current_previous(None, [])
        expected_result: RulesValuesDict = {"currentValue": None, "previousValues": []}
        self.assertDictEqual(expected_result, none_added)

    def test_remove_mode(self):
        """Tests when add_codes is false"""
        remove_result = set_current_previous(
            "anything", ["something", "and another"], add_codes=False
        )
        expected_result: RulesValuesDict = {
            "currentValue": None,
            "previousValues": ["something", "and another"],
        }
        self.assertDictEqual(expected_result, remove_result)

    def test_iterable_within_previous(self):
        """Tests that TypeError is raised by iterable within previous"""
        self.assertRaises(TypeError, set_current_previous, "anything", [(1, 2)])

    def test_prev_contains_int(self):
        """Tests that int is a valid type within previous values"""
        int_in_previous = set_current_previous([1, 2, 3], [123])
        expected_result: RulesValuesDict = {
            "currentValue": [1, 2, 3],
            "previousValues": [123],
        }
        self.assertDictEqual(expected_result, int_in_previous)
