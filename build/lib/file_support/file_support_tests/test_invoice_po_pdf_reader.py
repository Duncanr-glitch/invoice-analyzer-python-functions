"""Unittests for determining pdf asset type"""
from pathlib import PurePath
import unittest

from file_support.invoice_po_pdf_reader import determine_asset_type


class TestDetermineAssetType(unittest.TestCase):
    """Tests determining whether a pdf is the correct analysis type."""

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.set_pdf_path = (
            lambda path_addition="": f"showID/{path_addition}/file_name.pdf"
            if path_addition
            else "file_name.pdf"
        )

    def test_explicit_invoice(self):
        """Tests when invoice pdf type is explicitly defined"""
        invoice_path = PurePath(self.set_pdf_path("invoice"))
        self.assertEqual("invoice", determine_asset_type(invoice_path))

    def test_explicit_po(self):
        """Tests when purchase order pdf type is explicitly defined"""
        invoice_path = PurePath(self.set_pdf_path("purchaseOrder"))
        self.assertEqual("purchaseOrder", determine_asset_type(invoice_path))

    def test_implicit_invoice_bad_asset_type(self):
        """Tests when invoice pdf type isn't explicitly defined because of non existent asset type"""
        invoice_path = PurePath(self.set_pdf_path("asldfjkaslk"))
        self.assertEqual("invoice", determine_asset_type(invoice_path))

    def test_implicit_invoice_length_2(self):
        """Tests when invoice pdf type isn't explicitly defined because it is length 1"""
        invoice_path = PurePath(self.set_pdf_path())
        self.assertEqual("invoice", determine_asset_type(invoice_path))

    def test_purchase_order_min_length(self):
        """Tests when purchase order is in the path and the length is 2"""
        invoice_path = PurePath("purchaseOrder/test.pdf")
        self.assertEqual("purchaseOrder", determine_asset_type(invoice_path))
