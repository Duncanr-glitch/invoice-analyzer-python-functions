# type: ignore[reportInvalidStringEscapeSequence]
"""Functions for handling budget rules files"""

from typing import Iterable, Optional, TypedDict, Union
from collections import defaultdict, deque
from datetime import datetime, date
from io import BytesIO
from pathlib import PurePath
import re

from openpyxl import load_workbook

from general.string_formatter_functions import format_cateogry_str
from general.date_range import DateRange


class RulesValuesDict(TypedDict):
    """Type for the value dict I send to firestore"""

    currentValue: any
    previousValues: list


class DateFromUntil(TypedDict):
    """Type for the date from until conversion"""

    dateFrom: list[datetime | str | None]
    dateUntil: list[datetime | str | None]


def set_previous_values(new_value, existing_previous_values: RulesValuesDict) -> list:
    """Sets the new previous values list."""
    current_value = existing_previous_values.get("currentValue")
    if current_value is not None and current_value != new_value:
        if isinstance(current_value, Iterable):
            current_value = str(current_value)

        if isinstance(new_value, Iterable):
            new_value = str(new_value)

        previous_values = deque(
            [
                item
                for item in existing_previous_values.get("previousValues", [])
                if item != new_value
            ],
            maxlen=10,
        )

        previous_values.appendleft(current_value)
        previous_values = list(previous_values)
    else:
        previous_values = existing_previous_values.get("previousValues", [])
        if new_value in previous_values:
            previous_values.pop(previous_values.index(new_value))
    return previous_values


def set_current_previous(
    new_value: any, previous_values: list, add_codes: bool = True
) -> Optional[RulesValuesDict]:
    """Sets new current and previous"""
    if any(
        isinstance(prev_val, Iterable) and not isinstance(prev_val, str)
        for prev_val in previous_values
    ):
        raise TypeError(
            f"Previous Values can't contain a non-string iterable: {previous_values}"
        )
    if add_codes:
        if new_value is not None:
            return {"currentValue": new_value, "previousValues": previous_values}

        if not previous_values:
            return {"currentValue": None, "previousValues": []}

        return None

    return {"currentValue": None, "previousValues": previous_values}


def date_value_scans(
    spreadsheet: DateFromUntil,
    index: int,
    existing_values: RulesValuesDict,
    add_codes: bool = True,
) -> Union[RulesValuesDict, None]:
    """Handles the rule analysis for the date range."""

    def convert_possible_datestring(
        date_string: Union[datetime, str, None]
    ) -> Union[datetime, str, None]:
        """Tries to convert the date value to datetime"""
        if date_string is not None:
            if not isinstance(date_string, (datetime, date)):
                date_parts = re.sub(
                    "\s+", "", str(date_string).replace("/", "-")
                ).split("-")
                if len(date_parts[0]) == 2:
                    date_parts[0] = "20" + date_parts[0]
                date_val = "-".join(date_parts)

                try:
                    date_val = datetime.strptime(date_val, "%Y-%m-%d")
                except ValueError:
                    date_val = ""

                return date_val or date_string

        return date_string

    try:
        date_from = convert_possible_datestring(spreadsheet.get("dateFrom", [])[index])
    except IndexError:
        date_from = None
    try:
        date_until = convert_possible_datestring(
            spreadsheet.get("dateUntil", [])[index]
        )
    except IndexError:
        date_until = None

    if date_from and date_until:
        try:
            date_range = DateRange(lower_date=date_from, upper_date=date_until)
            date_range = date_range.strftime("%Y-%m-%d")
        except TypeError:
            date_range = f"{date_from},{date_until}"
    else:
        date_range = None
    return set_current_previous(
        date_range, set_previous_values(date_range, existing_values), add_codes
    )


def create_modify_rules_data(
    spreadsheet: BytesIO,
    new_file_name: PurePath,
    existing_rules_dict: Optional[dict] = None,
    add_codes: bool = True,
) -> defaultdict:
    """Iterates through the new spreadsheet for new rules."""
    # Load the workbook into openpyxl
    workbook = load_workbook(spreadsheet)

    # Valid categories
    category_headings = ["location", "date", "vendor", "category"]

    # Formats each column into a dictionary with the column headers as keys
    sheet_cols = {
        format_cateogry_str(str(col[0].value or "")): [cell.value for cell in col[1:]]
        for col in workbook.active.iter_cols()
        if col[0].value is not None
    }

    # Set each category heading to the list of headings for the coding values
    coding_headings = {
        category_heading: [
            code_heading
            for code_heading in sheet_cols.keys()
            if category_heading in code_heading.lower()
            and code_heading != category_heading
        ]
        for category_heading in category_headings
    }
    current_datetime = datetime.today()

    full_rules_dict = defaultdict(
        lambda: defaultdict(lambda: defaultdict(dict)), existing_rules_dict or {}
    )
    for category_heading in category_headings:
        category_values = sheet_cols.get(category_heading, [])
        for index, value in enumerate(category_values):
            if value is not None or not add_codes:
                new_value_key = re.sub("\s+", " ", str(value))
                if category_heading == "date":
                    new_date_dict = full_rules_dict["date"]["dateRange"]
                    new_date_range = date_value_scans(
                        sheet_cols, index, new_date_dict[new_value_key], add_codes
                    )
                    if new_date_range is not None:
                        new_date_dict[new_value_key] = new_date_range
                    else:
                        new_date_dict.pop(new_value_key, None)
                else:
                    for associated_heading in coding_headings[category_heading]:
                        new_dictionary = full_rules_dict[category_heading][
                            associated_heading
                        ]
                        new_value = sheet_cols[associated_heading][index]

                        if (
                            associated_heading == "possibleVendorCategories"
                            and new_value is not None
                        ):
                            new_value = re.split(
                                ",[\s+]?", re.sub("\s+", " ", str(new_value))
                            )
                        elif isinstance(new_value, str):
                            new_value = re.sub("\s+", " ", new_value)

                        previous_values = set_previous_values(
                            new_value, new_dictionary[new_value_key]
                        )

                    new_current_prev = set_current_previous(
                        new_value, previous_values, add_codes
                    )
                    if new_current_prev is not None:
                        new_dictionary[new_value_key] = new_current_prev
                    else:
                        new_dictionary.pop(new_value_key, None)

                    # if add_codes:
                    #     if new_value is not None:
                    #         new_dictionary[new_value_key]: RulesValuesDict = {
                    #             "currentValue": new_value,
                    #             "previousValues": previous_values
                    #         }
                    #     elif not previous_values:
                    #         new_dictionary[new_value_key]: RulesValuesDict = {
                    #             "currentValue": None,
                    #             "previousValues": []
                    #         }
                    #     else:
                    #         new_dictionary.pop(new_value_key)
                    # else:
                    #     new_dictionary[new_value_key]: RulesValuesDict = {
                    #         "currentValue": None,
                    #         "previousValues": previous_values
                    #     }

    full_rules_dict["lastUpdateDate"] = full_rules_dict["entryDate"] = current_datetime

    full_rules_dict["activeStartDate"] = full_rules_dict.get(
        "activeStartDate", current_datetime
    )

    file_list = full_rules_dict.get("files", [])
    if new_file_name.name not in file_list:
        file_list = list(deque(file_list + [new_file_name.name], maxlen=10))
    full_rules_dict["files"] = file_list

    return full_rules_dict
