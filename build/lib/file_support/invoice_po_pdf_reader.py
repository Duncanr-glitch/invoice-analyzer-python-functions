"""Reads PDF Invoices/PurchaseOrders
and extracts text or sends them to Google OCR"""

from collections import OrderedDict
from uuid import uuid4, UUID
from datetime import datetime
from io import BytesIO
from pathlib import PurePath
from typing import Literal, Union

# from firebase_admin import storage, firestore

from PyPDF2 import PdfReader

AssetTypeLiterals = Literal["invoice", "purchaseOrder"]


def determine_asset_type(file_path: PurePath) -> AssetTypeLiterals:
    """Determines what asset type the image or pdf represents from the lowest level folder"""
    asset_types: set[AssetTypeLiterals] = {"invoice", "purchaseOrder"}
    if len(file_path.parts) >= 2 and file_path.parent.name in asset_types:
        return file_path.parent.name
    return "invoice"


def invoice_po_pdf_reader(new_pdf: BytesIO, bucket_name: str, new_file_path: PurePath):
    """Takes in a file and extracts text from pdf or image"""
    print("reading begins")
    # Save the pdf pages text with the file name uuid4
    uuid_doc_id = new_file_path.stem.split("+", maxsplit=1)[0]
    # Check that the file path actually has uuid4
    try:
        UUID(uuid_doc_id)
        uuid_exists_in_file_path = True
    except ValueError:
        uuid_doc_id = str(uuid4())
        uuid_exists_in_file_path = False
    # Determine whether the pdf is an invoice or a purchase order
    asset_type = determine_asset_type(new_file_path)
    # Access pdf from firestore to read with PyPDF2

    invoice_pdf = PdfReader(new_pdf)

    # Collecting dictionaries for text and images
    invoice_page_text: Union[OrderedDict, str] = OrderedDict()
    invoice_page_images = {}

    # Read each pdf page and concatenate all text togther
    for index, page in enumerate(invoice_pdf.pages, 1):
        page_text = page.extract_text().replace("\n", " ")
        if page_text or not page.images:
            invoice_page_text[str(index)] = page_text
        else:
            page_image = page.images[0]
            invoice_page_images[index] = page_image

    # Write the text found to the extractedText collection with the file location
    # This is the collection Google OCR writes to
    if invoice_page_text:
        firestore_db = firestore.client()
        firestore_entry = {
            "file": f"gs://{bucket_name}/{new_file_path.name}",
            "entryDate": datetime.today(),
        }

        # If there are no images to OCR, save the extracted text directly
        if not invoice_page_images:
            extracted_text_collection = firestore_db.collection(
                f"{asset_type}ExtractedText"
            )
            invoice_page_text = "\n".join(invoice_page_text.values())
        # Otherwise save the fragments to the collecting document
        else:
            extracted_text_collection = firestore_db.collection(
                f"{asset_type}PdfExtractedText"
            )

        # Add text to the firestore dictionary
        firestore_entry["text"] = invoice_page_text

        # Set the new document
        extracted_text_collection.document(uuid_doc_id).set(firestore_entry)
    # If no text was found in some pages,
    # Upload the pdf pages as images in the same directory as the pdf
    if invoice_page_images:
        # Get the storage bucket for OCR Images
        image_storage_bucket = storage.bucket("invoice-analyze-images")
        # Upload the unreadable page images to Firebase Storage
        # For OCR reading
        for page, img in invoice_page_images.items():
            # Add index and page length to end of file
            # If the file correctly has a uuid in path
            if uuid_exists_in_file_path:
                image_name = f"{new_file_path}-{page}-{len(invoice_pdf.pages)}.png"
            else:
                # Otherwise insert the uuid into the image path to track
                inserted_uuid_name: Union[list, str] = new_file_path.name.rsplit(
                    "/", maxsplit=1
                )
                # Add back the folders if they were used
                if len(inserted_uuid_name) == 2:
                    inserted_uuid_name = (
                        f"{inserted_uuid_name[0]}/{uuid_doc_id}+{inserted_uuid_name[1]}"
                    )
                else:
                    inserted_uuid_name = f"{uuid_doc_id}+{inserted_uuid_name[0]}"
                image_name = f"{inserted_uuid_name}-{page}-{len(invoice_pdf.pages)}.png"

            # Save the image to the temp BytesIO object
            img_bytesio = BytesIO(img.data)
            # Save to Firebase Storage
            image_storage_bucket.blob(image_name).upload_from_file(
                img_bytesio, content_type="image/png"
            )
            print(f"Image file {image_name} upload succeeded!")
