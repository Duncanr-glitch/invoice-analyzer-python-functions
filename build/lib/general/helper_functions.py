"""Function/Method Performance Decorator"""
from functools import wraps
import time


def performance_timer(func):
    """Wraps a function or method for performance"""

    @wraps(func)
    def performance_timer_wrapper(*args, **kwargs):
        """Measures a function or method's execution time"""
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        # first item in the args, ie `args[0]` is `self`
        print(f"Function {func.__name__}{args} {kwargs} Took {total_time:.4f} seconds")
        return result

    return performance_timer_wrapper
