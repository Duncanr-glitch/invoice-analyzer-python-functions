"""DateRange Class"""
from typing import Union
from datetime import date, datetime, timedelta
from dataclasses import dataclass


@dataclass(order=True)
class DateRange:
    """Class for range of datetime objects"""

    lower_date: date
    upper_date: date

    @classmethod
    def strpdaterange(cls, date_range_string: str, format: str):
        """Creates DateRange object from 2 comma-delimeted dates"""
        try:
            lower_date, upper_date = date_range_string.split(",")[0:2]
        except ValueError as err:
            err.add_note("Less than 2 dates found. In the comma-delimited string.")
            raise

        lower_date = datetime.strptime(lower_date, format)
        upper_date = datetime.strptime(upper_date, format)
        return cls(lower_date, upper_date)

    def strftime(self, format: str):
        """Strings both lower_date and upper_date with a comma in between"""
        return f"{self.lower_date.strftime(format)},{self.upper_date.strftime(format)}"

    @staticmethod
    def _convert_to_date(value: Union[datetime, date]) -> date:
        """Converts datetime object into date object"""
        if isinstance(value, datetime):
            value = value.date()
        return value

    @staticmethod
    def _reverse_date_edges(lower: date, upper: date):
        if lower > upper:
            return upper, lower
        return lower, upper

    def move_upper_forward(self, value: timedelta):
        """Moves the upper_date value into the future"""
        self.upper_date += value

    def move_lower_forward(self, value: timedelta):
        """Moves the lower_date value into the future"""
        self.lower_date += value

    def move_upper_back(self, value: timedelta):
        """Moves the upper_date value into the past"""
        self.move_upper_forward(-value)

    def move_lower_back(self, value: timedelta):
        """Moves the lower_date value into the past"""
        self.move_lower_forward(-value)

    def __setattr__(self, attr: str, value: Union[datetime, date]) -> None:
        if attr in ["lower_date", "upper_date"]:
            if not isinstance(value, (datetime, date)):
                raise TypeError(
                    f"""{type(value)} is an invalid type for {attr}.
                                Please enter datetime or date."""
                )
        new_date = self._convert_to_date(value)
        try:
            if attr == "lower_date":
                new_dates = (new_date, self.upper_date)
            elif attr == "upper_date":
                new_dates = (self.lower_date, new_date)
            else:
                new_dates = []
            if new_dates:
                (
                    self.__dict__["lower_date"],
                    self.__dict__["upper_date"],
                ) = self._reverse_date_edges(*new_dates)
        except KeyError:
            self.__dict__[attr] = new_date

    def __getattr__(self, attr: str) -> timedelta:
        if attr == "range":
            if self.lower_date is not None and self.upper_date is not None:
                return (self.upper_date - self.lower_date) + timedelta(1)
            return timedelta(0)
        return self.__dict__[attr]

    def __contains__(self, date_value: Union[datetime, date]) -> bool:
        date_value = self._convert_to_date(date_value)
        return self.lower_date <= date_value <= self.upper_date

    def __hash__(self) -> int:
        return hash((self.lower_date, self.upper_date))

    def __len__(self):
        return abs(self.range.days)

    def __str__(self) -> str:
        return f"range from {self.lower_date} until {self.upper_date}"
