"""Module for categorizing and organizing invoice data with Palm 2"""
from typing import TypedDict, Union, Literal, Optional
from io import BytesIO
import json

from openpyxl import Workbook
from openpyxl.styles import Alignment
import pdfkit

# import google.generativeai as palm


# Type for adding specific inputs to Palm
class InputOutputs(TypedDict):
    """Type for adding data to palm prompts"""

    input: str
    output: str


# Types for the dictionary typings for the invoice Palm promptings
class InvoiceProduct(TypedDict):
    """Type for palm product categorization"""

    product: str | None
    category: str | None


# Type for Invoice Amount both total and line
class InvoiceValues(TypedDict):
    """Type for palm getting values"""

    value: int | None
    currency: str | None
    discount: float | None
    post_discount: int | None


class InvoiceAmt(TypedDict):
    """Type for palm getting total and itemized amounts"""

    total: InvoiceValues
    itemized: list[InvoiceValues]


class InvoiceLocation(TypedDict):
    """Type for palm getting location"""

    city: str | None
    state: str | None
    country: str | None
    address: str | None


class InvoiceLocationDict(TypedDict):
    company_address: InvoiceLocation
    customer_address: InvoiceLocation


class InvoiceDate(TypedDict):
    """Type for palm getting date"""

    year: int | None
    month: int | None
    day: int | None


class InvoiceVendor(TypedDict):
    """Type for palm getting the vendor"""

    vendor: str | None
    in_vendor_list: bool


DocumentTypes = Literal["invoice", "purchase order"]


class PalmCategorization:
    """For organizing all the Palm question and answer generation for invoices and purchase orders."""

    def __init__(
        self,
        invoice_text: Optional[str] = None,
        purchase_order_text: Optional[str] = None,
    ) -> None:
        """Sets up Palm connection and default variables for text generation."""
        if invoice_text:
            self.invoice_text = invoice_text
        else:
            self.invoice_text = ""

        if purchase_order_text:
            self.purchase_order_text = purchase_order_text
        else:
            self.purchase_order_text = ""
        palm.configure(api_key="AIzaSyBuYFxM_bkrkfLfEXhjCj8VzUBJDbD6Dts")
        self.defaults = {
            "model": "models/text-bison-001",
            "temperature": 0.25,
            "candidate_count": 1,
            "top_k": 40,
            "top_p": 0.95,
            "max_output_tokens": 1024,
            "stop_sequences": [],
        }
        self.film_industry_boilerplate = (
            "This invoice is charged to a film or tv production."
        )

    def _determine_document_text(self, document_type: DocumentTypes):
        document_text_dict: dict[DocumentTypes, str] = {
            "invoice": self.invoice_text,
            "purchase order": self.purchase_order_text,
        }

        return document_text_dict[document_type]

    @staticmethod
    def add_input_output_training(
        prompt: str, training_data: list[InputOutputs]
    ) -> str:
        """Adds set inputs and outputs to a Palm Prompt."""
        trainig_input_outputs = ""
        # Loops through training data to format into a string to add to the prompt.
        for data in training_data:
            trainig_input_outputs += (
                f"/ninput: {data['input']}/noutput:{data['output']}"
            )
        prompt += trainig_input_outputs
        return prompt

    def palm_response(self, prompt: str, new_input: str, return_json: bool = True):
        """Pings Palm to respond about the invoice in question"""
        result = palm.generate_text(
            **self.defaults, prompt=f"{prompt}/input: {new_input}/noutput: "
        ).result

        # If Palm gives back a JSON-ic dictionary, convert it from JSON to python dictionary
        if return_json:
            print(
                result.replace("'", '"')
                .replace("'null'", "null")
                .replace('"null"', "null")
            )
            try:
                return json.loads(
                    result.replace("'", '"')
                    .replace("'null'", "null")
                    .replace('"null"', "null")
                )
            except json.decoder.JSONDecodeError as json_err:
                # Raise a palm specific error since the default is to return pythonified dictionary
                # but palm returns a string by default
                raise ValueError(
                    f"""Palm's response isn't in a proper JSON Format

Palm Response: {result}"""
                ) from json_err

        return result

    def get_product(
        self,
        document_type: DocumentTypes,
        possible_categories: Optional[list[str]] = None,
    ) -> InvoiceProduct:
        """Categorizes an invoice's/purchase order's product(s) with the film industry in mind"""
        prompt = f"""{self.film_industry_boilerplate}
        What is/are the product(s) this {document_type} is for?
        What category does this product belong in? {f"Only use these categories: {possible_categories}. If it doesn't fit one of these categories, return null. Don't return any other value." if possible_categories else ''}
        return a JSON with the keys product, `and `category.
        If no product is found, return {{"product": null, "category": null}}"""

        return self.palm_response(prompt, self._determine_document_text(document_type))

    def get_document_number(
        self, number_type: DocumentTypes, document_type: Optional[DocumentTypes] = None
    ) -> Optional[str]:
        """Ascertains the invoice/purchase order number"""
        document_type = document_type or number_type
        null_return = {"segments": [], "separator": ""}
        prompt = f"""What is the {number_type} number for this {document_type}? Return it as JSON with segments key that contains the different components of the id as a python list and a separator key for character that separates the segments.
        The separator for the {number_type} number must be directly connected to the number.
        {f"If the purchase order number is the same as the invoice number, return {null_return}" if document_type == 'invoice' and number_type == 'purchase order' else ""}
        If no {number_type} number is found, return {null_return}"""

        invoice_number_parts = self.palm_response(
            prompt, self._determine_document_text(document_type)
        )

        if invoice_number_parts["segments"]:
            return invoice_number_parts["separator"].join(
                invoice_number_parts["segments"]
            )

        return None

    def get_amt(
        self,
        document_type: DocumentTypes,
        possible_categories: Optional[list[str]] = None,
    ) -> InvoiceAmt:
        """Gets the total and itemized amounts from the invoice/purchase order"""
        # Prompt template string for getting amounts
        general_prompt = (
            lambda return_type: f"""What is/are the product(s) this {document_type} is for?
        Categorize this product. {f"Only use these categories: {possible_categories}. If it doesn't fit one of these categories, return null. other is not a valid category." if possible_categories else ''}
        Give me the answer as a JSON {return_type} with the keys of product (as a string), category (as a string if it exists, otherwise null), value (as an integer), currency, discount (as a decimal), and post_discount (as an integer)."""
        )

        # Prompts for total and itemized parts of the receipt
        # Total is a single dictionary, Itemized a llist of dictionaries
        total_amount_prompt = f"""What is the total amount for this {document_type}?
        {general_prompt('dictionary')}
        If no product is found, return {{"product": null, "category": null, "value": null, "currency": null, "discount": null, "post_discount": null}}."""
        itemized_lines_prompt = f"""Itemize this {document_type}. {general_prompt('list with each line item as a dictionary')}
        If no items are found, return []."""

        total_amt = self.palm_response(
            total_amount_prompt, self._determine_document_text(document_type)
        )
        itemized = self.palm_response(
            itemized_lines_prompt, self._determine_document_text(document_type)
        )

        return {"total": total_amt, "itemized": itemized}

    def get_description(
        self,
        document_type: DocumentTypes,
        description_format: Union[dict, str, None] = None,
    ) -> str:
        """Has Palm describe the invoice/purchase order according to the given format with the film industry in mind"""
        prompt = f"""{self.film_industry_boilerplate}
        Describe this {document_type} for an accountant on the production.
        {f'''Use the following format:
        {description_format}''' if description_format else ''}
        Follow this format strictly."""
        return self.palm_response(
            prompt, self._determine_document_text(document_type), return_json=False
        )

    def get_location(self, document_type: DocumentTypes) -> InvoiceLocationDict:
        """Gets the unabbreviated location in pieces for both customer and company"""
        # Templated string for both company and customer prompts
        prompt_skeleton = (
            lambda location_type: f"""Get the {location_type} address of this {document_type} as JSON with keys of the full name of the city, state, country, and address. Convert all city, state, and country abbreviations.
        If you don't find an address, return {{"city": null, "state": null, "country": null, "address": null}}"""
        )
        customer_prompt = prompt_skeleton("customer")
        company_prompt = prompt_skeleton("company")

        # Pings Palm for the two locations
        customer_location = self.palm_response(
            customer_prompt, self._determine_document_text(document_type)
        )
        company_location = self.palm_response(
            company_prompt, self._determine_document_text(document_type)
        )
        return {
            "company_address": company_location,
            "customer_address": customer_location,
        }

    def get_date(self, document_type: DocumentTypes) -> InvoiceDate:
        """Gets the invoice/purchase order date(s) in its date pieces"""
        prompt = f"""Get the Year, Month, and Day of the {document_type} as JSON.
        If no date is found, return {{"year": null, "month": null, "day": null}}"""
        return self.palm_response(prompt, self._determine_document_text(document_type))

    def get_vendor(
        self, document_type: DocumentTypes, possible_vendors: Optional[list[str]] = None
    ) -> dict:
        """Gets the vendor of the invoice/purchase order"""
        # Sets up finding variables
        possible_vendors = possible_vendors or []
        vendor_prompt_general = f"{f' Only use these vendor companies to determine the vendor company: {possible_vendors}' if possible_vendors else ''}. return as a JSON dictionary with the key vendor."
        found_vendors = []
        # Loop through the vendors to see if an exact match exists in the invoice
        for vendor in possible_vendors:
            if vendor in self.invoice_text:
                found_vendors.append(vendor)

        # If more than one is found, let palm arbitrate
        if len(found_vendors) > 1:
            get_most_probable_prompt = f"Which of these vendors is most likely to have charged this {document_type}?{vendor_prompt_general}"
            found_vendors = self.palm_response(
                get_most_probable_prompt, self._determine_document_text(document_type)
            ).get("vendor", "")
        # When only one is found, set that as the vendor
        elif len(found_vendors) == 1:
            found_vendors = found_vendors[0]
        # If no exact matches are found, let palm determine who the vendor is
        else:
            full_find_prompt = f"""What vendor company charged this {document_type}?{vendor_prompt_general}
            If no vendor is found, return {{"vendor": null}}."""

            found_vendors = self.palm_response(
                full_find_prompt, self._determine_document_text(document_type)
            ).get("vendor", "")

        # If palm returns something other than a string as the vendor
        if type(found_vendors) != str:
            # Check the name key for the vendor
            try:
                found_vendors = found_vendors.get("name", "")
            # Otherwise we can't find the vendor and return blank
            except KeyError:
                found_vendors = ""

        # Return the vendor and whether its in the possible_vendors
        return {
            "vendor": (found_vendors or ""),
            "in_vendor_list": found_vendors in possible_vendors,
        }

    @staticmethod
    def generate_results_output(
        title: str,
        columns: list[str],
        data: list[list],
        output_format: Literal["pdf", "excel"],
    ) -> Optional[BytesIO]:
        """Creates html to pdf or excel sheet for the data
        returns it in BytesIO form to be processed elsewhere"""
        # Get the number of columns
        row_len = len(columns)
        # Add blank cells to the data if its too short
        data = [
            row + ["" for _ in range(row_len - len(row))] if len(row) < row_len else row
            for row in data
        ]
        # Return an HTML Table for pdf and an .xlsx file for excel
        if output_format == "pdf":
            # Get the templated HTML table as a string
            output_grid = """
<head>
    <style>
        table, tr, th, td {
            outline: 1px solid black;
        }
        table {
            margin: auto;
        }
        th {
            text-align: center;
        }
    </style>
</head>

<html>
    <body>
        <div>
            <table>
                {{TableTitle}}
                <tr>
                    {{ColumnHeaders}}
                </tr>
                {{DataRows}}
            </table>
        </div>
    </body>
</html>"""

            # Set the table title with column width spanning the whole table
            output_grid = output_grid.replace(
                "{{TableTitle}}", f"<th colspan='{row_len}'>{title}</th>"
            )

            # Set table column headers and join them together
            table_columns = [f"<th>{col}</th>" for col in columns]
            table_columns = "\n\t\t    ".join(table_columns)
            output_grid = output_grid.replace("{{ColumnHeaders}}", table_columns)

            # Organize data into table cells
            table_rows = [f"<td>{cell}</td>" for row in data for cell in row]
            # Organize each cell into the correct row
            table_rows = [
                f"<tr>\n\t\t\t{cell}"
                if index % row_len == 0
                else f"{cell}\n\t\t</tr>"
                if index % row_len == row_len - 1
                else cell
                for index, cell in enumerate(table_rows)
            ]
            table_rows = "\n\t\t\t".join(table_rows)

            output_grid = output_grid.replace("{{DataRows}}", table_rows)

            # Return a BytesIO object
            output_pdf = BytesIO(pdfkit.from_string(output_grid))  # type: ignore[reportGeneralTypeIssues]
            return output_pdf

        elif output_format == "excel":
            # Centered text alignment
            centered_cell = Alignment(horizontal="center")

            # Variables for adding data to the spreadsheet
            output_workbook = Workbook()
            active_sheet = output_workbook.active
            if active_sheet:
                left_title_cell = active_sheet.cell(1, 1)  # type: ignore[reportOptionalMemberAccess]

                # Setting up the title of the sheet
                left_title_cell.value = title
                left_title_cell.alignment = centered_cell
                # Merge the title across the column length
                active_sheet.merge_cells(start_row=1, start_column=1, end_row=1, end_column=row_len)  # type: ignore[reportOptionalMemberAccess]

                # Iterate through columns and data to add them to the sheet
                for row_index, row in enumerate([columns] + data, 2):
                    active_sheet.append(row)  # type: ignore[reportOptionalMemberAccess]
                    if row_index == 2:
                        # Center the column rows' text
                        for column_index, _ in enumerate(row, 1):
                            active_sheet.cell(row_index, column_index).alignment = centered_cell  # type: ignore[reportOptionalMemberAccess]

                # Save excel sheet to BytesIO and return
                output_excel = BytesIO()
                output_workbook.save(output_excel)
                return output_excel


if __name__ == "__main__":
    test_invoice = "ABC DesignBill to:John Smith442 Swansea Street, Denver, CO 80303United StatesDescriptionDesign projectQuantityPay online12Terms & ConditionsNet 30.Please make the payment via the payment link below.10% discount applied for new customer.Go to: https://invoice.sumup.com/s/F8wY5gBRkOr scan the QR code using your phone's camera.ABC Design 3839 Maxwell Street - Denver, CO 80303Email: info@abcdesigners.com Phone: 303-555-0173Website: abcdesigners.comUnithourDiscount 10%Subtotal without TaxTotal USDInvoice:Invoice Date:Due Date:Amount PaidAmount Due (USD)Price50.002021-00505.21.202106.20.2021Amount600.00-60.00600.00540.000.00540.00sumupPage 1 of 1 for Invoice #2021-005"
    # test_invoice = "ABC DesignBill to:John Smith"
    test_po = "[Company Name][Street Address][City, ST ZIP]Phone: (000) 000-0000Fax: (000) 000-0000Website:VENDOR[Company Name][Contact or Department][Street Address][City, ST ZIP]Phone: (000) 000-0000Fax: (000) 000-0000REQUISITIONERITEM #[23423423][45645645]SHIP VIAProduct XYZProduct ABCDESCRIPTIONComments or Special InstructionsThank you for your business.PURCHASE ORDERhttps://www.vertex42.com/ExcelTemplates/excel-purchase-order.htmlF.O.B.SHIP TO[Name][Company Name][Street Address][City, ST ZIP][Phone]QTY151DATEPO #SHIPPING TERMSUNIT PRICE150.0075.00SUBTOTALTAXSHIPPINGOTHERTOTALIf you have any questions about this purchase order, please contact9/29/2015[123456]TOTAL2,250.0075.002,325.00$2,325.00Purchase Order Template Ⓒ 2015 Vertex42.com"
    test_po = "Invoice ToMax ElectronicsA 204,Shivaji Nagar, BengaluruGSTIN/UIN: 29AAACP7879D1Z0State Name: Karnataka, Code : 29Contact: 9810123456E-Mail: Max@guruelectronics.comwww.maxelectronics.comDespatch ToMax ElectronicsA 204,Shivaji Nagar, Bengalurue-mail: max@guruelectronics.comGSTIN/UINState Name: 29AAACP7879D1Z0: Karnataka, Code: 29SupplierA-One Electronics261, 11th MainM.G RoadBengaluruGSTIN/UINState NameSINo.: 27AAACP98765A1Z: Karnataka, Code : 29Description of Goods1 Asus 16.6 inch Monitor2 Dell 17 Inch MonitorPURCHASE ORDERAmount Chargeable (in words)INR Seventy Three Thousand Four Hundred OnlyVoucher No.1Supplier's Ref./Order No.PO/001Despatch throughTerms of DeliveryDelivery at Ex-FactoryTotalDue on Quantity31-Mar-202031-Mar-202012 NosDated29-Mar-2020Mode/Terms of PaymentPayment Via ChequeOther Reference(s)Prepared byThis is a Computer Generated DocumentDestination5,300.00 Nos5 Nos7 Nos 6,700.00 NosRate perVerified byAmount26,500.0046,900.00€73,400.00E. & O.Efor Max ElectronicsAuthorised Signatory"
    palm_object = PalmCategorization(
        invoice_text=test_invoice, purchase_order_text=test_po
    )

    format_1 = """Company:
    Amount:
    Product: """
    invoice_product = palm_object.get_product(
        "invoice", possible_categories=["food & drink", "consultation"]
    )
    invoice_num = palm_object.get_document_number("invoice")
    purchase_order_num_on_invoice = palm_object.get_document_number(
        number_type="purchase order", document_type="invoice"
    )
    invoice_amounts = palm_object.get_amt(
        "invoice", possible_categories=["food & drink", "rental cars"]
    )
    invoice_description = palm_object.get_description("invoice", format_1)
    invoice_location = palm_object.get_location("invoice")
    invoice_date = palm_object.get_date("invoice")
    invoice_vendor = palm_object.get_vendor(
        "invoice", possible_vendors=["Something", "BCD Definitions"]
    )

    purchase_order_product = palm_object.get_product(
        "purchase order", possible_categories=["food & drink", "consultation"]
    )
    purchase_order_num = palm_object.get_document_number("purchase order")
    purchase_order_amounts = palm_object.get_amt(
        "purchase order", possible_categories=["food & drink", "rental cars", "gear"]
    )
    purchase_order_description = palm_object.get_description("purchase order", format_1)
    purchase_order_location = palm_object.get_location("purchase order")
    purchase_order_date = palm_object.get_date("purchase order")
    purchase_order_vendor = palm_object.get_vendor(
        "purchase order", possible_vendors=["Something", "BCD Definitions"]
    )

    print(
        f"""Invoice Results
    Product: {invoice_product}

    Invoice #: {invoice_num}

    Purchase Order #: {purchase_order_num_on_invoice}

    Total Amount: {invoice_amounts["total"]}

    Itemized Invoice: {invoice_amounts["itemized"]}

    Invoice Description:
    {invoice_description}

    Location: {invoice_location}

    Date: {invoice_date}

    Vendor: {invoice_vendor}"""
    )

    print(
        f"""Purchase Order Results
    Product: {purchase_order_product}

    Purchase Order #: {purchase_order_num}

    Total Amount: {purchase_order_amounts["total"]}

    Itemized Invoice: {purchase_order_amounts["itemized"]}

    Invoice Description:
    {purchase_order_description}

    Location: {purchase_order_location}

    Date: {purchase_order_date}

    Vendor: {purchase_order_vendor}"""
    )

    test_pdf = palm_object.generate_results_output(
        columns=["first", "second", "third"],
        data=[["one", "two"], ["another", "again", "and now"]],
        output_format="pdf",
        title="Invoice Analysis",
    )
    test_xls = palm_object.generate_results_output(
        columns=["first", "second", "third"],
        data=[["one", "two"], ["another", "again", "and now"]],
        output_format="excel",
        title="Invoice Analysis",
    )
    with open("test_output.pdf", "wb") as f:
        f.write(test_pdf.getbuffer())  # type: ignore[reportOptionalMemberAccess]
    with open("test_output.xls", "wb") as f:
        f.write(test_xls.getbuffer())  # type: ignore[reportOptionalMemberAccess]
