"""Unittets for the DateRange Class"""
import unittest
from datetime import datetime, timedelta
from random import randint
from typing import TypedDict

from general.date_range import DateRange


class DateRangeValues(TypedDict):
    """Type for the values of DateRange"""

    range: timedelta
    len: int
    lower: datetime.date
    upper: datetime.date


class DateRangeSetup(TypedDict):
    """Type for the setup for DateRange tests"""

    date_range: DateRange
    basic_attrs: DateRangeValues
    boundary_move: timedelta


class TestDateRange(unittest.TestCase):
    """Unittests for range of dates class"""

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.today = datetime.today()

    def _date_range_setup_boundary_move(self) -> DateRangeSetup:
        test_range = randint(1, 100)
        basic_lower = self.today.date()
        basic_upper = basic_lower + timedelta(test_range)
        basic_date_range = DateRange(basic_lower, basic_upper)
        test_range += 1
        basic_attribute_dict: DateRangeValues = {
            "range": timedelta(test_range),
            "len": test_range,
            "lower": basic_lower,
            "upper": basic_upper,
        }

        # Move date boundary value
        boundary_move = timedelta(randint(1, 50))

        return {
            "date_range": basic_date_range,
            "basic_attrs": basic_attribute_dict,
            "boundary_move": boundary_move,
        }

    def test_basic_date_range(self):
        """Makes sure the basic daterange is setup correctly"""
        date_range_setup = self._date_range_setup_boundary_move()
        basic_date_range = date_range_setup["date_range"]
        basic_expected_attribute_dict = date_range_setup["basic_attrs"]
        actual_attribute_dict: DateRangeValues = {
            "range": basic_date_range.range,
            "len": len(basic_date_range),
            "lower": basic_date_range.lower_date,
            "upper": basic_date_range.upper_date,
        }
        self.assertDictEqual(basic_expected_attribute_dict, actual_attribute_dict)

    def test_move_upper_forward(self):
        """Test move upper forward"""
        date_range_setup = self._date_range_setup_boundary_move()
        basic_date_range = date_range_setup["date_range"]
        boundary_move = date_range_setup["boundary_move"]
        basic_upper = date_range_setup["basic_attrs"]["upper"]

        basic_date_range.move_upper_forward(boundary_move)
        self.assertEqual(basic_upper + boundary_move, basic_date_range.upper_date)

    def test_move_upper_back(self):
        """Test move upper back"""
        date_range_setup = self._date_range_setup_boundary_move()
        basic_date_range = date_range_setup["date_range"]
        boundary_move = date_range_setup["boundary_move"]

        date_range_attributes = date_range_setup["basic_attrs"]
        new_upper_date = date_range_attributes["upper"] - boundary_move
        range_attr = date_range_attributes["range"]

        basic_date_range.move_upper_back(boundary_move)

        if boundary_move > range_attr - timedelta(1):
            self.assertEqual(new_upper_date, basic_date_range.lower_date)
        else:
            self.assertEqual(new_upper_date, basic_date_range.upper_date)

    def test_move_lower_forward(self):
        """Test move lower forward"""
        date_range_setup = self._date_range_setup_boundary_move()
        basic_date_range = date_range_setup["date_range"]
        boundary_move = date_range_setup["boundary_move"]

        date_range_attributes = date_range_setup["basic_attrs"]
        new_lower_date = date_range_attributes["lower"] + boundary_move
        range_attr = date_range_attributes["range"]

        basic_date_range.move_lower_forward(boundary_move)

        if boundary_move > range_attr - timedelta(1):
            self.assertEqual(new_lower_date, basic_date_range.upper_date)
        else:
            self.assertEqual(new_lower_date, basic_date_range.lower_date)

    def test_move_lower_back(self):
        """Test move lower back"""
        date_range_setup = self._date_range_setup_boundary_move()
        basic_date_range = date_range_setup["date_range"]
        boundary_move = date_range_setup["boundary_move"]
        basic_lower = date_range_setup["basic_attrs"]["lower"]

        basic_date_range.move_lower_back(boundary_move)
        self.assertEqual(basic_lower - boundary_move, basic_date_range.lower_date)

    def test_mixed_up_boundaries(self):
        """Tests where the upper and lower are initialized backwards"""
        lower = (self.today - timedelta(randint(1, 50))).date()
        upper = (self.today + timedelta(randint(1, 50))).date()
        real_range = upper - lower + timedelta(1)
        backwards_date_range = DateRange(upper, lower)

        actual_range_attrs: DateRangeValues = {
            "lower": backwards_date_range.lower_date,
            "upper": backwards_date_range.upper_date,
            "range": backwards_date_range.range,
            "len": len(backwards_date_range),
        }
        expected_range_attrs: DateRangeValues = {
            "lower": lower,
            "upper": upper,
            "range": real_range,
            "len": real_range.days,
        }
        self.assertDictEqual(expected_range_attrs, actual_range_attrs)

    def test_string_initialization(self):
        """Tests that DateRange can be initialized by a string"""
        format = "%Y-%m-%d"
        lower = self.today - timedelta(randint(1, 50))
        upper = self.today + timedelta(randint(1, 50))
        real_range = upper - lower + timedelta(1)

        stringed_date_range = DateRange.strpdaterange(
            f"{lower.strftime(format)},{upper.strftime(format)}", format=format
        )

        actual_range_attrs: DateRangeValues = {
            "lower": stringed_date_range.lower_date,
            "upper": stringed_date_range.upper_date,
            "range": stringed_date_range.range,
            "len": len(stringed_date_range),
        }
        expected_range_attrs: DateRangeValues = {
            "lower": lower.date(),
            "upper": upper.date(),
            "range": real_range,
            "len": real_range.days,
        }
        self.assertDictEqual(expected_range_attrs, actual_range_attrs)

    def test_return_date_range_str(self):
        """Tests strfttime method"""
        format = "%y/%m/%d"
        upper = self.today + timedelta(1)
        date_range_to_string = DateRange(self.today, upper)

        self.assertEqual(
            f"{self.today.strftime(format)},{upper.strftime(format)}",
            date_range_to_string.strftime(format=format),
        )

    def test_contains_magic_meth(self):
        """Tests true and false states of the in operator"""
        date_range_setup = self._date_range_setup_boundary_move()
        date_range = date_range_setup["date_range"]
        range_len = date_range_setup["basic_attrs"]["len"]

        contained_in = self.today + timedelta(randint(1, range_len - 1))
        contained_outside_top = self.today + timedelta(
            randint(range_len, range_len + 50)
        )
        contained_outside_bottom = self.today - timedelta(randint(1, 50))

        self.assertTrue(contained_in in date_range)
        self.assertFalse(contained_outside_top in date_range)
        self.assertFalse(contained_outside_bottom in date_range)

    def test_cant_intialize_int(self):
        """Tests that an error is raised when
        anything beside datetime/dates are entered"""
        self.assertRaises(TypeError, DateRange, randint(1, 50), randint(1, 50))

    def test_cant_initialize_part(self):
        """Test partial error with only one property wrong"""
        self.assertRaises(
            TypeError, DateRange, self.today + timedelta(randint(1, 50)), {1: 2}
        )
