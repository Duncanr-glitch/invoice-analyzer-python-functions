"""Unit Tests for the location matching function"""
import unittest

from general.location_matching import LocationDict, location_matching


class TestLocationMatching(unittest.TestCase):
    """Tests matching a location string to a location dict"""

    def test_location_with_just_city(self):
        """Tests when the location string just has a city"""
        location_string = "Madison"
        location_dict: LocationDict = {
            "city": "Madison",
            "country": {"full_name": "United States of America", "abbreviation": "USA"},
            "state": {"full_name": "Wisconsin", "abbreviation": "WI"},
        }
        location_result = location_matching(location_string, location_dict)
        expected_result: LocationDict = {"city": "Madison"}
        self.assertDictEqual(expected_result, location_result)

    def test_location_with_state_match(self):
        """Tests when state full string matches
        Also tests when state is a string as input rather than dict"""
        location_string = "Boston, Massachusets"
        location_dict: LocationDict = {
            "city": "Salem",
            "state": "Massachusets",
            "country": {"full_name": "United States of America", "abbreviation": "USA"},
        }
        location_result = location_matching(location_string, location_dict)
        expected_result: LocationDict = {"state": "Massachusets"}
        self.assertDictEqual(expected_result, location_result)

    def test_location_with_country_abbr_match(self):
        """Tests matching on the country abbreviation"""
        location_string = "Paris, FR"
        location_dict: LocationDict = {
            "city": "Normandy",
            "country": {"abbreviation": "FR", "full_name": "France"},
        }
        location_result = location_matching(location_string, location_dict)
        expected_result: LocationDict = {"country": "France"}
        self.assertDictEqual(expected_result, location_result)

    def test_location_with_two_matches(self):
        """Tests when multiple levels of location match -- lowest level succeeds"""
        location_string = "Arkansas, USA"
        location_dict: LocationDict = {
            "city": "Little Rock",
            "state": {"full_name": "Arkansas", "abbreviation": "AR"},
            "country": {"full_name": "United States", "abbreviation": "USA"},
        }
        location_result = location_matching(location_string, location_dict)
        expected_result: LocationDict = {"state": "Arkansas"}
        self.assertDictEqual(expected_result, location_result)

    def test_location_no_match(self):
        """Tests when there are no matches on the location string
        Also tests when state and country are strings as input rather than dicts"""
        location_string = "Gibberish is here"
        location_dict: LocationDict = {
            "city": "Kalamazoo",
            "state": "New Mexico",
            "country": "Canada",
        }
        location_result = location_matching(location_string, location_dict)
        self.assertIsNone(location_result)

    def test_no_match_lacking_one_key(self):
        """Tests when there should be no match and abbreviation or full_name is missing
        no match still appears"""
        location_string = "nothing will match"
        location_dict: LocationDict = {
            "state": {"full_name": "some locale"},
            "country": {"abbreviation": "USA"},
        }
        location_result = location_matching(location_string, location_dict)
        self.assertIsNone(location_result)

    def test_location_abbreviation_returned(self):
        """Tests that matching abbreviation is returned if no full_name is available"""
        location_string = "Indianapolis, IN"
        location_dict: LocationDict = {"state": {"abbreviation": "IN"}}
        location_result = location_matching(location_string, location_dict)
        expected_result: LocationDict = {"state": "IN"}
        self.assertDictEqual(expected_result, location_result)
