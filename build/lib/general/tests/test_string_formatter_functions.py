"""Unittests for string_formatter_functions"""
import unittest

from general.string_formatter_functions import format_cateogry_str


class TestFormatCategoryStr(unittest.TestCase):
    """UnitTests for formatting rules categories"""

    def test_strip_whitespace(self):
        """Stripping whitespace from the ends"""
        result = format_cateogry_str("  category  ")
        self.assertEqual(result, "category")

    def test_lowercase_conversion(self):
        """Converting mixed case properly"""
        result = format_cateogry_str("dATEnAME")
        self.assertEqual(result, "dateName")

    def test_singular_categories(self):
        """Categories is singularized"""
        result = format_cateogry_str("categories code")
        self.assertEqual(result, "categoryCode")

    def test_reverse_order(self):
        """Reversing names appropriately"""
        result = format_cateogry_str("categories reversed")
        self.assertEqual(result, "reversedCategory")

    def test_pluralize_possible_categories(self):
        """Making category plural with possible list"""
        result = format_cateogry_str("possible vendor category")
        self.assertEqual(result, "possibleVendorCategories")

    def test_camel_case_conversion_reverse(self):
        """Testing code reversal with no space"""
        result = format_cateogry_str("codelocation")
        self.assertEqual(result, "locationCode")

    def test_multi_middle_spaces(self):
        """Middle space appropriately handled"""
        result = format_cateogry_str("date        from")
        self.assertEqual(result, "dateFrom")

    def test_multi_middle_space_reverse(self):
        """Middle space appropriately handled with reverse"""
        result = format_cateogry_str("date        until")
        self.assertEqual(result, "dateUntil")

    def test_reverse_possible_categories(self):
        """Make sure a pure reversal of possible vendor categories works"""
        result = format_cateogry_str("categories vendor possible")
        self.assertEqual(result, "possibleVendorCategories")

    def test_middle_mixed_possible_categories(self):
        """Make sure other orders of possible vendor categories works"""
        result = format_cateogry_str("vendor categories possible")
        self.assertEqual(result, "possibleVendorCategories")

    def test_plural_vendor_category(self):
        """Make sure both vendor and categories are made singular/plural"""
        result = format_cateogry_str("possible vendors category")
        self.assertEqual(result, "possibleVendorCategories")

    def test_date_range_name_becomes_date(self):
        """Makes sure that the user readable date range name becomes just date"""
        result = format_cateogry_str("date range name")
        self.assertEqual(result, "date")


if __name__ == "__main__":
    unittest.main()
