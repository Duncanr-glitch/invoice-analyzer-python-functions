"""Package Setup File"""
from setuptools import setup

setup(name="invoice-python",
      version="1.0",
      description="Python Functions for Invoice Analyzer",
      author="Duncan Richards",
      author_email="duncan_richards12@yahoo.com",
      python_requires=">=3.11, <4"
    )
